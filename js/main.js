var app = new Vue({
    el: '#app',
    data: {
        errorMsg: "",
        successMsg: "",
        showAddClient: false,
        showEditClient: false,
        showDeleteClient: false,
        showAddOwner: false,
        showEditOwner: false,
        showDeleteOwner: false,
        showAddContract: false,
        showEditContract: false,
        showDeleteContract: false,
        showAddImmobile: false,
        showEditImmobile: false,
        showDeleteImmobile: false,
        showPayments: false,
        clients: [],
        owners: [],
        contracts: [],
        immobiles: [],
        payments: [],
        newClient: {name: "", email: "", phone: ""},
        newContract: {client_id: "", owner_id: "", immobile_id: "", start_date: "", end_date: "", rate_adm: "", rent: "", condominium: "", iptu: ""},
        newImmobiles: {address: "", owners_id: ""},
        newOwners: {name: "", email: "", phone: "", transfer_day: ""},
        newPayments: {contract_id: "", value: "", transfer_value: ""},
        currentClient: {},
        currentOwner: {},
        currentImmobile: {},
        currentContract: {},
        currentPayments: {},
    },
    mounted: function () {
        this.getAllClients();
        this.getAllOwners();
        this.getAllContracts();
        this.getAllImmobiles();
        this.getAllPayments();
    },
    methods: {
        getAllClients() {
            axios.get("http://localhost/vista/access.php?action=read&model=clients")
                .then (function (response) {
                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.clients = response.data.content;
                    }
                })
        },
        updateClient() {
            var frmData = app.toFormData(app.currentClient);

            axios.post("http://localhost/vista/access.php?action=update", frmData)
                .then (function (response) {
                    app.currentClient = {};

                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.successMsg = response.data.message;
                        app.getAllClients();
                    }
                });
        },
        deleteClient() {
            var frmData = app.toFormData(app.currentClient);

            axios.post("http://localhost/vista/access.php?action=delete", frmData)
                .then (function (response) {
                    app.currentClient = {};

                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.successMsg = response.data.message;
                        app.getAllClients();
                    }
                });
        },
        getAllPayments() {
            axios.get("http://localhost/vista/access.php?action=read&model=payments")
                .then (function (response) {
                    app.currentClient = {};

                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.payments = response.data.content;
                    }
                })
        },
        getAllOwners() {
            axios.get("http://localhost/vista/access.php?action=read&model=owners")
                .then (function (response) {
                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.owners = response.data.content;
                    }
                })
        },
        getAllContracts() {
            axios.get("http://localhost/vista/access.php?action=read&model=contracts")
                .then (function (response) {
                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.contracts = response.data.content;
                    }
                })
        },
        getAllImmobiles() {
            axios.get("http://localhost/vista/access.php?action=read&model=immobiles")
                .then (function (response) {
                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.immobiles = response.data.content;
                    }
                })
        },
        addClient() {
            var frmData = app.toFormData(app.newClient);
            axios.post("http://localhost/vista/access.php?action=create&model=clients", frmData)
                .then (function (response) {
                    app.newClient = {name: "", email: "", phone: ""};

                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.successMsg = response.data.message;
                        app.getAllClients();
                    }
                });
        },
        addContract() {
            var frmData = app.toFormData(app.newContract);
            axios.post("http://localhost/vista/access.php?action=create&model=contracts", frmData)
                .then (function (response) {
                    app.newContract = {client_id: "", immobile_id: "", owner_id: "", start_date: "",
                        end_date: "", rate_adm: "", rent: "", condominium: "", iptu: ""};

                    if (response.data.error) {
                        app.errorMsg = response.data.message;
                    } else {
                        app.successMsg = response.data.message;
                        app.getAllContracts();
                    }
                });
        },
        toFormData(obj) {
            var formData = new FormData();
            console.log(obj);
            for (var i in obj) {
                formData.append(i, obj[i]);
            }

            return formData;
        },
        selectClient (client) {
            app.currentClient = client;
        },
    }
});