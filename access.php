<?php

$conn = new mysqli("localhost", "vista", "vista", "test_vista");

if ($conn->connect_error) {
    die("Erro de conexão " . $conn->connect_error);
}

$result = array('error' => false);
$action = '';
$model = $_GET['model'];

if (isset($_GET['action'])) {
    $action = $_GET['action'];
}

if ($action == 'read') {
    $sql = $conn->query("SELECT * FROM $model");
    $data = array();

    while ($row = $sql->fetch_assoc()) {
        array_push($data, $row);
    }

    $result['content'] = $data;
}

if ($action == 'create') {

    if ($model == 'clients') {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];

        $sql = $conn->query("INSERT INTO clients (name, email, phone) 
                                VALUES('$name','$email','$phone')");
    }

    if ($model == 'owners') {
        $name = $_POST['name'];
        $email = $_POST['email'];
        $phone = $_POST['phone'];
        $transfer_day = $_POST['transfer_day'];

        $sql = $conn->query("INSERT INTO owners (name, email, phone, transfer_day) 
                                VALUES('$name','$email','$phone','$transfer_day')");
    }

    if ($model == 'immobiles') {
        $address = $_POST['address'];
        $owner = $_POST['owner'];

        $sql = $conn->query("INSERT INTO owners (address, owner) 
                                VALUES('$address','$owner')");
    }

    if ($model == 'contracts') {
        $payment        = payment($_POST);
        $transfer_value = transfer($_POST);

        $client_id = $_POST['client_id'];
        $immobile_id = $_POST['immobile_id'];
        $owner_id = $_POST['owner_id'];
        $start_date = $_POST['start_date'];
        $end_date = $_POST['end_date'];
        $rate_adm = $_POST['rate_adm'];
        $rent = $_POST['rent'];
        $condominium = $_POST['condominium'];
        $iptu = $_POST['iptu'];

        $sql = $conn->query("INSERT INTO contracts (client_id, immobile_id, owner_id, start_date, end_date, rate_adm,rent, condominium, iptu) 
                                VALUES('$client_id','$immobile_id','$owner_id','$start_date','$end_date','$rate_adm','$rent','$condominium','$iptu')");

        if ($sql) {
            $last_id = $conn->insert_id;

            $sql = $conn->query("INSERT INTO payments (contracts_id, value, transfer_value) 
                                VALUES('$last_id','$payment','$transfer_value')");
        }
    }

    if ($model == 'payments') {
        $contracts_id = $_POST['contracts_id'];
        $payment_status = $_POST['payment_status'];
        $transfer_status = $_POST['transfer_status'];
        $value = $_POST['value'];
        $transfer_value = $_POST['$transfer_value'];

        $sql = $conn->query("INSERT INTO owners (contracts_id, payment_status,value,transfer_value) 
                                VALUES('$contracts_id','$payment_status','$value','$transfer_value')");
    }

    if ($sql) {
        $result['message'] = "Informacao inserida com sucesso!";
    } else {
        $result['con'] = $conn->errno;
        $result['con'] = $conn->erro;
        $result['con'] = $conn->error_list;
        $result['error'] = true;
        $result['message'] = "Nao foi possivel inserir as informacoes! ";
    }
}

function payment($fields)
{
    $rent        = $fields['rent'] !== null ? $fields['rent'] : 0;
    $iptu        = $fields['iptu'] !== null ? $fields['iptu'] : 0;
    $condominium = $fields['condominium'] !== null ? $fields['condominium'] : 0;
    $timestamps  = date($fields['start_date']);
    $startDay    = date('w', $timestamps);

    if ($startDay !== 1 || $startDay !== 01) {
        $monthDays   = cal_days_in_month(CAL_GREGORIAN,date('m'),date('Y'));
        $currentDays = $monthDays - $startDay;
        $value       = ($rent / $monthDays) * $currentDays;

        $rent = number_format($value,2,',','');
    }

    return $rent + $iptu + $condominium;
}

function transfer($fields)
{
    return payment($fields) - $fields['rate_adm'];
}

if ($action == 'update') {
    $id = $_POST['id'];
    $name = $_POST['name'];
    $phone = $_POST['phone'];
    $email = $_POST['email'];

    $sql = $conn->query("
                        UPDATE clients 
                        SET name='$name', email='$email', phone='$phone'
                        WHERE id = '$id'");

    if ($sql) {
        $result['message'] = "Cliente alterado com sucesso!";
    } else {
        $result['error'] = true;
        $result['message'] = "Não foi possível alterar o cliente!";
    }
}

if ($action == 'delete') {
    $id = $_POST['id'];

    $sql = $conn->query("
                        DELETE FROM clients
                        WHERE id = '$id'");

    if ($sql) {
        $result['message'] = "Cliente removido com sucesso!";
    } else {
        $result['error'] = true;
        $result['message'] = "Não foi possível remover o cliente!";
    }
}

$conn->close();
echo json_encode($result);

